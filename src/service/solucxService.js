class service {

    static getBases(){

        var bases = [];
        $.ajax({
            type: "GET",
            url: 'http://services.solucx.com.br/mock/bases',
            async: false,
            dataType: "json",
            success: (data) => {
              bases = data;
            },
            error: function(error) {
              console.log(error);
            }
        });

        return bases;
    }

}

export default service;